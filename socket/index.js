import { Server } from 'socket.io';

// create socket server
const io = new Server(9004, {
  cors: {
    origin: '*',
    // origin: 'http://localhost:3000'
  },
});

// users online array
let users = [];

// check if user is online and added him to users online array
export const addUser = (userData, socketId) => {
  !users.some((user) => user.sub === userData.sub) &&
    users.push({ ...userData, socketId });
};

// check if user is online and return data if exist
const getUser = (receiverId) => {
  return users.find((user) => user.sub === receiverId);
};

// return all users online if senderId, receiverId
const getAllUsers = (senderId, receiverId) => {
  return users.filter(
    (user) => user.sub === senderId || user.sub === receiverId
  );
};

// ********** SERVER **********
// start server
io.on('connection', (socket) => {
  console.log('*** connection socket server');

  // set users online
  socket.on('addUsers', (userData) => {
    // add user to online
    addUser(userData, socket.id);

    console.log(`*** user ${userData.name} connected`);
    console.log(`*** online guests`, users);

    // get all guests
    io.emit('getUsers', users);
  });

  // send message
  socket.on('sendMessage', (data) => {
    // get online user
    const users = getAllUsers(data.senderId, data.receiverId);

    console.log('---- data ', data);
    console.log('---- users ', users);
    // console.log('---- socket ', socket.id);

    // send online user event new message
    if (users.length) {
      users.forEach((user) => {
        io.to(user.socketId).emit('getMessage', data);
      });
    }
  });

  // disconnect user
  socket.on('disconnect', () => {
    const updatedUsers = users.filter((user) => user.socketId !== socket.id);
    console.log('*** user disconnected', updatedUsers);
    users = updatedUsers;
  });
});
