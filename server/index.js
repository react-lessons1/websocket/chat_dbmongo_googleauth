import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import morgan from 'morgan';

import { route } from './routes/route.js';

const app = express();

// images to server
app.use(express.static('public'));
app.use('/images', express.static('images'));

/************* MIDDLEWARE *************/
// use cors for connect any IP
app.use(cors());

// morgan HTTP logger
app.use(morgan(':method :url :status :res[content-length] :response-time ms'));

// use json format data from web-client
// для разбора данных в формате JSON
// no param - limit: '30mb'
// app.use(express.json());

// encode POST request data from form
// для разбора URL-кодированных данных
// no param - limit: '30mb'
// app.use(express.urlencoded({ extended: true }));

// bodyParser
// use json format data from web-client
// для разбора данных в формате JSON
app.use(bodyParser.json({ limit: '30mb', extended: true }));
// encode POST request data from form
// для разбора URL-кодированных данных
app.use(bodyParser.urlencoded({ limit: '30mb', extended: true }));

// load dotenv constants
dotenv.config();
/**************************************/

// start server
mongoose
  .connect(process.env.MONGO_DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() =>
    app.listen(process.env.PORT, () => {
      console.log(`*** server start on ${process.env.PORT}`);
    })
  )
  .catch((error) => console.log(`*** error ${error}`));

// use routes
app.use('/', route);

app.use('/', (res, req) => {
  req.send('hello');
});
