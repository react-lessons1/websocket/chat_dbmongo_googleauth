import { userModel } from './../model/userModel.js';

export const addUser = async (req, res) => {
  try {
    const isExist = await userModel.findOne({ sub: req.body.sub });

    // check is user exist in DB
    if (isExist) {
      return res.status(200).json({ msg: 'user already exist' });
    }

    // else create new user in DB
    const newUser = new userModel(req.body);

    // save user to DB
    await newUser.save();

    //
    res.status(200).json(newUser);
    //
  } catch (error) {
    console.log('error addUser', error);
    res.status(500).json({ msg: `error  addUser: ${error}` });
  }
};

export const getGuests = async (req, res) => {
  try {
    const guests = await userModel.find({});
    res.status(200).json(guests);
  } catch (error) {
    res.status(500).json({ msg: `error  getGuests: ${error}` });
  }
};
