import { messageModel } from './../model/messageModel.js';
import { conversationModel } from './../model/conversationModel.js';

export const addMessage = async (req, res) => {
  try {
    // else create new message in DB
    const newMessage = new messageModel(req.body);

    // save message to DB
    await newMessage.save();

    // update new messages in conversation DB
    await conversationModel.findByIdAndUpdate(req.body.conversationId, {
      message: req.body.text,
    });

    //
    res.status(200).json('Message has been sent successfully');
    //
  } catch (error) {
    console.log('error addMessage', error);
    res.status(500).json({ msg: `error  addMessage: ${error}` });
  }
};

export const getMessages = async (req, res) => {
  try {
    // update new messages in conversation DB
    const messages = await messageModel.find({ conversationId: req.params.id });

    //
    res.status(200).json(messages);
    //
  } catch (error) {
    console.log('error getMessage', error);
    res.status(500).json({ msg: `error  getMessage: ${error}` });
  }
};
