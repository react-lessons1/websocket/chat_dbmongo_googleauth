import { conversationModel } from '../model/conversationModel.js';

// create/find conversation
export const addConversation = async (req, res) => {
  try {
    // get params from request
    const senderId = req.body.senderId;
    const receiverId = req.body.receiverId;

    // check if conversation is exist
    const isExist = await conversationModel.findOne({
      members: { $all: [senderId, receiverId] },
    });

    // send response if conversation exist
    if (isExist) {
      return res.status(200).json('conversation already exist');
    }

    // else create new conversation
    const newConversation = new conversationModel({
      members: [senderId, receiverId],
    });

    // save in DB new conversation
    await newConversation.save();
    //
    res.status(200).json('conversation saved successfully');
    //
  } catch (error) {
    console.log('error addConversation', error);
    res.status(500).json({ msg: `error addConversation: ${error}` });
  }
};

// send conversation detail to client
export const getConversation = async (req, res) => {
  try {
    // get params from request
    const senderId = req.body.senderId;
    const receiverId = req.body.receiverId;

    // check if conversation is exist
    const conversation = await conversationModel.findOne({
      members: { $all: [senderId, receiverId] },
    });

    // send response if conversation exist
    if (conversation) {
      return res.status(200).json(conversation);
    }
    //
    res.status(200).json('conversation not found');
    //
  } catch (error) {
    console.log('error getConversation', error);
    res.status(500).json({ msg: `error getConversation: ${error}` });
  }
};
