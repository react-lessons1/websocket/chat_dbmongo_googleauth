import express from 'express';

import { addUser, getGuests } from '../controllers/userControllers.js';
import {
  addConversation,
  getConversation,
} from '../controllers/conversationController.js';
import { addMessage, getMessages } from '../controllers/messageController.js';

export const route = express.Router();

route.post('/add', addUser);
route.get('/guests', getGuests);

route.post('/conversation/add', addConversation);
route.post('/conversation/get', getConversation);

route.post('/message/add', addMessage);
route.get('/message/:id', getMessages);
