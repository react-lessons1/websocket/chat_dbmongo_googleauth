import mongoose from 'mongoose';

const userModelSchema = mongoose.Schema(
  {
    sub: {
      type: String,
      require: true,
    },
    email: {
      type: String,
    },
    name: {
      type: String,
    },
    picture: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

export const userModel = mongoose.model('user', userModelSchema);
