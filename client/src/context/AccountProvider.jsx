import { createContext, useRef, useState, useEffect } from 'react';

import { io } from 'socket.io-client';

// create context
export const AccountContext = createContext(null);

export const AccountProvider = ({ children }) => {
  const [account, setAccount] = useState(null);
  const [person, setPerson] = useState({});
  const [conversation, setConversation] = useState({});
  const [activeGuests, setActiveGuests] = useState([]);

  const socket = useRef();

  // connection socket server
  useEffect(() => {
    socket.current = io('ws://localhost:9004');
  }, []);

  return (
    <AccountContext.Provider
      value={{
        socket,
        account,
        setAccount,
        person,
        setPerson,
        conversation,
        setConversation,
        activeGuests,
        setActiveGuests,
      }}
    >
      {children}
    </AccountContext.Provider>
  );
};
