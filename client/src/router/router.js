import { createBrowserRouter } from 'react-router-dom';

import { App } from 'src/components/App';

import { Messenger } from 'src/components/Messenger/Messenger';

export const router = createBrowserRouter([
  {
    element: <App />,
    path: '/',
    errorElement: <div>Error 404</div>,
    children: [
      {
        element: <Messenger />,
        index: true,
      },
    ],
  },
]);
