import axios from 'axios';

const url = 'http://localhost:5004';

// add user to DB
export const addUser = async (data) => {
  try {
    await axios.post(`${url}/add`, data);
  } catch (error) {
    console.log('Error addUser api', error);
  }
};

// get all users added in DB
export const getGuests = async () => {
  try {
    const { data } = await axios.get(`${url}/guests`);
    return data;
  } catch (error) {
    console.log('Error getUsers api', error);
  }
};

// create chat
export const setConversation = async (data) => {
  try {
    await axios.post(`${url}/conversation/add`, data);
  } catch (error) {
    console.log('Error setConversation api', error);
  }
};

// get detail chat
export const getConversation = async (obj) => {
  try {
    const { data } = await axios.post(`${url}/conversation/get`, obj);
    return data;
  } catch (error) {
    console.log('Error getConversation api', error);
  }
};

// send message to DB
export const newMessage = async (objMessage) => {
  try {
    const { data } = await axios.post(`${url}/message/add`, objMessage);
    return data;
  } catch (error) {
    console.log('Error newMessage api', error);
  }
};

// get messages from DB
export const getMessages = async (conversationId) => {
  try {
    const { data } = await axios.get(`${url}/message/${conversationId}`);
    return data;
  } catch (error) {
    console.log('Error getMessages api', error);
  }
};
