import { Box, Dialog, List, ListItem, Typography, styled } from '@mui/material';
import { GoogleLogin } from '@react-oauth/google';
import jwtDecode from 'jwt-decode';
import { useContext } from 'react';
import { addUser } from 'src/api/connection';

import { qrCodeImage } from 'src/api/constants';
import { AccountContext } from 'src/context/AccountProvider';

const dialogStyle = {
  position: 'absolute',
  bottom: '-50px',
  height: '90%',
  width: '70%',
  maxWidth: '98%',
  maxHeight: '98%',
  overflow: 'hidden',
  borderRadius: '18px',
  padding: '44px',
};

const Component = styled(Box)(({ theme }) => ({
  display: 'flex',
  gap: 12,
  justifyContent: 'space-between',
}));

const Container = styled(Box)`
  padding: 0;
`;

const StyledList = styled(List)`
  & > li {
    padding: 0;
    margin: 4px 0;
  }
`;

const QRCode = styled('img')({
  height: 256,
  width: 256,
});

const GoogleBox = styled(Box)`
  & iframe {
    margin: 0 auto !important;
  }
`;

export const LoginDialog = () => {
  // set authorization in context
  const { setAccount } = useContext(AccountContext);

  // google authorization ***************
  const onLoginSuccess = async (res) => {
    // console.log('Google login', res);

    // data of user from google
    const decode = jwtDecode(res.credential);
    console.log('Google login', decode);
    await setAccount(decode);

    // set user to DB
    addUser(decode);
  };

  const onLoginError = (res) => {
    console.log('Google login error', res);
  };
  // google authorization ***************

  return (
    <Dialog open={true} PaperProps={{ sx: dialogStyle }} hideBackdrop={true}>
      <Component>
        <Container>
          <Typography variant="h6">To use our app on your computer:</Typography>
          <StyledList>
            <ListItem>1. Lorem ipsum dolor sit amet.</ListItem>
            <ListItem>2. Lorem ipsum dolor sit amet.</ListItem>
            <ListItem>3. Lorem ipsum dolor sit amet.</ListItem>
          </StyledList>
        </Container>
        <Box>
          <QRCode src={qrCodeImage} alt="qr code" />
        </Box>
      </Component>
      <Box sx={{ margin: '24px auto' }}>
        <Typography variant="span">- OR -</Typography>
      </Box>
      <GoogleBox sx={{ margin: '0px auto' }}>
        <GoogleLogin onSuccess={onLoginSuccess} onError={onLoginError} />
      </GoogleBox>
    </Dialog>
  );
};
