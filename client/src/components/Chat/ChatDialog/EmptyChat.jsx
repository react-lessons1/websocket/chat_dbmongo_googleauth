import { Box } from '@mui/material';

export const EmptyChat = () => {
  return (
    <Box sx={{ margin: '0 auto', fontSize: '22px', paddingTop: '50px' }}>
      Invite a guest to start a chat
    </Box>
  );
};
