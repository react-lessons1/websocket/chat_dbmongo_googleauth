import { Box, styled } from '@mui/material';
import { useContext, useEffect, useState } from 'react';

import { getMessages } from 'src/api/connection';
import { AccountContext } from 'src/context/AccountProvider';
import { Message } from './Message';
import { useRef } from 'react';

const Container = styled(Box)`
  border-bottom: 1px solid #f6f6f6;
  display: flex;
  flex-direction: column;
  flex: 1 0 auto;
  height: 1vh;
  overflow: scroll;
  padding: 0 6px 12px 0;
  gap: 8px;
`;

export const Conversation = () => {
  const { person, conversation, socket } = useContext(AccountContext);

  const [messages, setMessages] = useState([]);
  const [incomingMessages, setIncomingMessages] = useState(null);

  // ************* GET SOCKET EVENT ABOUT NEW MESSAGE *************
  useEffect(() => {
    socket.current.on('getMessage', (data) => {
      console.log('getMessage', data);

      setIncomingMessages({
        ...data,
        createdAt: Date.now(),
      });
    });
  }, []);
  // ************* GET SOCKET EVENT ABOUT NEW MESSAGE *************

  // ************* CONVERSATION *************
  // get all message in conversation
  useEffect(() => {
    const getMessagesDetail = async () => {
      const data = await getMessages(conversation._id);
      setMessages(data);
    };

    // if conversation not empty/exist
    conversation._id && getMessagesDetail();
  }, [conversation._id, person._id]);
  // ************* CONVERSATION *************

  // ************* SCROLL *************
  // setting browser-scroll to useRef() to automatically scroll when message is outside
  const scrollRef = useRef();

  // always scroll to last message
  useEffect(() => {
    scrollRef.current?.scrollIntoView({ behavior: 'smooth' });
  }, [messages]);
  // ************* SCROLL *************

  // ************* GET NEW MESSAGE FROM SOCKET *************
  // set new message from Socket server when arrive event getMessage
  useEffect(() => {
    incomingMessages &&
      conversation?.members?.includes(incomingMessages.senderId) &&
      setMessages((prev) => [...prev, incomingMessages]);
  }, [incomingMessages, conversation]);
  // ************* GET NEW MESSAGE FROM SOCKET *************

  return (
    <Container>
      {messages &&
        messages.map((message, index) => (
          <Box ref={scrollRef} key={index}>
            <Message message={message} />
          </Box>
        ))}
    </Container>
  );
};
