import { Box, styled } from '@mui/material';
import { useContext, useState, useEffect } from 'react';

import { Conversation } from './Conversation';
import { Sender } from './Sender';
import { AccountContext } from 'src/context/AccountProvider';
import { getConversation } from 'src/api/connection';

const Container = styled(Box)`
  display: flex;
  flex-direction: column;
  /* flex: 1 0 auto; */
  width: 100%;
  gap: 8px;
`;

export const ChatDialog = () => {
  // get data to chat with
  const { person, account, setConversation } = useContext(AccountContext);

  // get conversation id from DB
  useEffect(() => {
    const getConversationDetails = async () => {
      const data = await getConversation({
        senderId: account.sub,
        receiverId: person.sub,
      });
      setConversation(data);
    };
    getConversationDetails();
  }, [person.sub]);

  return (
    <Container>
      <Conversation />
      <Sender />
    </Container>
  );
};
