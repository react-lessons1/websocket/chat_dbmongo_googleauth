import { Box, InputBase, styled } from '@mui/material';
import EmojiEmotionOutlinedIcon from '@mui/icons-material/EmojiEmotionsOutlined';
import AttachFile from '@mui/icons-material/AttachFile';
import Mic from '@mui/icons-material/Mic';
import { useState, useContext } from 'react';

import { AccountContext } from 'src/context/AccountProvider';
import { newMessage } from 'src/api/connection';

const Container = styled(Box)`
  display: flex;
  align-items: center;
  flex: 0 1 auto;
  gap: 8px;
  padding: 4px 16px;
`;

const InputField = styled(InputBase)`
  background-color: #f6f6f6;
  border-radius: 16px;
  padding: 0px 16px;
  width: 100%;
`;

export const Sender = () => {
  // get person to chat with
  const { person, account, conversation, socket } = useContext(AccountContext);

  const [messageText, setMessageText] = useState('');

  // set send text
  const sendText = async (e) => {
    const code = e.keyCode || e.which;
    if (code === 13) {
      let message = {
        senderId: account.sub,
        receiverId: person.sub,
        conversationId: conversation._id,
        type: 'text',
        text: messageText,
      };

      // console.log(message);

      // send message to DB
      await newMessage(message);

      // send event about new message to Socket server
      socket.current.emit('sendMessage', message);

      // clear sender input
      setMessageText('');
    }
  };

  return (
    <Container>
      <EmojiEmotionOutlinedIcon sx={{ color: '#81bbaf', cursor: 'pointer' }} />
      <AttachFile sx={{ color: '#81bbaf', cursor: 'pointer' }} />
      {/* <Box> */}
      <InputField
        value={messageText}
        placeholder="Type your message"
        onChange={(e) => setMessageText(e.target.value)}
        onKeyPress={(e) => sendText(e)}
      />
      {/* </Box> */}
      <Mic sx={{ color: '#81bbaf', cursor: 'pointer' }} />
    </Container>
  );
};
