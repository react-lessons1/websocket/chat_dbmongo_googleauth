import { Box, Dialog, styled } from '@mui/material';
import { ChatDialog } from './ChatDialog/ChatDialog';
import { ChatMenu } from './ChatMenu/ChatMenu';
import { EmptyChat } from './ChatDialog/EmptyChat';
import { useContext } from 'react';
import { AccountContext } from 'src/context/AccountProvider';

const dialogStyle = {
  height: '90%',
  width: '90%',
  maxWidth: '98%',
  maxHeight: '98%',
  minWidth: '700px',
  borderRadius: '18px',
  padding: '8px',
};

const Container = styled(Box)`
  display: flex;
  flex: 1 0 auto;
  gap: 8px;
`;

const LeftContainer = styled(Box)`
  width: 260px;
  min-width: 260px;
  background-color: #8fabd347;
  padding: 8px;

  border-top-left-radius: 12px;
  border-bottom-left-radius: 12px;

  display: flex;
  flex-direction: column;
  flex: 1 0 auto;
  gap: 4px;
`;

const RightContainer = styled(Box)`
  width: 100%;
  min-width: 300px;
  background-color: #95c3b952;
  padding: 8px;

  border-top-right-radius: 12px;
  border-bottom-right-radius: 12px;

  display: flex;
  gap: 4px;
`;

export const Chat = () => {
  const { person } = useContext(AccountContext);
  return (
    <Dialog open={true} PaperProps={{ sx: dialogStyle }} hideBackdrop={true}>
      <Container>
        <LeftContainer>
          <ChatMenu />
        </LeftContainer>
        <RightContainer>
          {Object.keys(person).length ? <ChatDialog /> : <EmptyChat />}
        </RightContainer>
      </Container>
    </Dialog>
  );
};
