import { Box, InputBase, styled } from '@mui/material';
import { Search as SearchIcon } from '@mui/icons-material';

const InputField = styled(InputBase)`
  background-color: #f6f6f6;
  border-radius: 16px;
  padding: 0px 5px 0px 34px;
  width: 100%;
`;

const Icon = styled(Box)`
  position: absolute;
  color: #d8d8d8;
  top: 5px;
  left: 7px;
  z-index: 2;
`;

const BoxSearch = styled(Box)`
  position: relative;
  width: 100%;
  padding-bottom: 12px;
  margin-bottom: 12px;
  border-bottom: 1px solid #f6f6f6;
`;

export const Search = ({ setSearchText }) => {
  return (
    <BoxSearch>
      <Icon>
        <SearchIcon />
      </Icon>
      <InputField
        placeholder="Search ..."
        onChange={(e) => setSearchText(e.target.value)}
      />
    </BoxSearch>
  );
};
