import { useState } from 'react';

import { UserDataHeader } from './UserDataHeader';
import { Search } from './Search';
import { Guests } from './Guests/Guests';

export const ChatMenu = () => {
  const [searchText, setSearchText] = useState('');

  return (
    <>
      <UserDataHeader />
      <Search setSearchText={setSearchText} />
      <Guests searchText={searchText} />
    </>
  );
};
