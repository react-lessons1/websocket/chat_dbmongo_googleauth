import { Box, styled } from '@mui/material';
import { useContext } from 'react';
import { setConversation } from 'src/api/connection';

import { AccountContext } from 'src/context/AccountProvider';

const Container = styled(Box)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 6px;
  margin-bottom: 6px;
  border-bottom: 1px solid #f6f6f6;
  cursor: pointer;
`;

const UserData = styled(Box)`
  display: flex;
  gap: 8px;
`;

const AvatarImg = styled('img')`
  width: 52px;
  height: 52px;
  border-radius: 50%;
`;

export const Guest = ({ guest }) => {
  // set person to chat with
  const { setPerson, account, activeGuests } = useContext(AccountContext);

  const getGuests = async () => {
    // set person to chat with to context
    setPerson(guest);

    // create chat in DB
    await setConversation({ senderId: account.sub, receiverId: guest.sub });
  };

  return (
    <Container onClick={() => getGuests()}>
      <UserData>
        <AvatarImg src={guest.picture} alt="avatar" />
        <Box sx={{ fontSize: '12px' }}>
          <Box sx={{ fontWeight: 'bold' }}>{guest.name}</Box>
          <Box>{guest.email}</Box>
          <Box sx={{ textAlign: 'right' }}>
            {activeGuests?.find((active) => active.sub === guest.sub) ? (
              <Box sx={{ color: '#4eb077' }}>online</Box>
            ) : (
              <Box sx={{ color: '#b84f4f' }}>offline</Box>
            )}
          </Box>
        </Box>
      </UserData>
    </Container>
  );
};
