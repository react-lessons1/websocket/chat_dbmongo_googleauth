import { Box } from '@mui/material';
import { useEffect, useState } from 'react';

import { useContext } from 'react';

import { getGuests } from 'src/api/connection';
import { Guest } from './Guest';
import { AccountContext } from 'src/context/AccountProvider';

export const Guests = ({ searchText }) => {
  const [guests, setGuests] = useState([]);

  // get user data from context
  const { account, socket, setActiveGuests } = useContext(AccountContext);

  // set guests in State from DB or Search input filtering
  useEffect(() => {
    const fetchGuests = async () => {
      // get guests from DB
      const response = await getGuests();

      // filter guest if search exist
      const filteredData = response.filter((guest) =>
        guest.name.toLowerCase().includes(searchText.toLowerCase())
      );

      if (filteredData) setGuests(filteredData);
    };
    //
    fetchGuests();
  }, [searchText]);

  // set User to socket server in online
  useEffect(() => {
    socket.current.emit('addUsers', account);
    socket.current.on('getUsers', (users) => {
      setActiveGuests(users);
    });
  }, [account, setActiveGuests, socket]);

  return (
    <Box sx={{ marginTop: '20px' }}>
      {guests.map(
        (guest) =>
          guest.sub !== account.sub && <Guest key={guest.sub} guest={guest} />
      )}
    </Box>
  );
};
