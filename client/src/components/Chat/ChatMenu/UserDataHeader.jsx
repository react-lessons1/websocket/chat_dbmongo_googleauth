import { Box, Menu, MenuItem, styled } from '@mui/material';
import { useState, useContext } from 'react';
import MoreVertOutlinedIcon from '@mui/icons-material/MoreVertOutlined';

import { AccountContext } from 'src/context/AccountProvider';

const Container = styled(Box)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 6px;
  margin-bottom: 6px;
  border-bottom: 1px solid #f6f6f6;
`;

const UserData = styled(Box)`
  display: flex;
  gap: 8px;
`;

const AvatarImg = styled('img')`
  width: 52px;
  height: 52px;
  border-radius: 50%;
`;

const UserProfile = styled(MoreVertOutlinedIcon)`
  color: #adadad;
  cursor: pointer;
`;

const MenuItemStyled = styled(MenuItem)`
  font-size: 14px;
  padding: 4px 18px;
`;

export const UserDataHeader = () => {
  // get user data from context
  const { account } = useContext(AccountContext);

  // set open menu
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOpen = (e) => {
    setAnchorEl(e.currentTarget);
  };

  return (
    <Container>
      <UserData>
        <AvatarImg src={account.picture} alt="avatar" />
        <Box sx={{ fontSize: '12px' }}>
          <Box sx={{ fontWeight: 'bold' }}>{account.name}</Box>
          <Box>{account.email}</Box>
        </Box>
      </UserData>

      <UserProfile onClick={handleOpen} />
      <Menu
        id="basic-menu"
        keepMounted
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        // getContentAnchorE1={null}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        MenuListProps={{
          'arial-labelledby': 'basic-button',
        }}
      >
        <MenuItemStyled onClick={handleClose}>Profile</MenuItemStyled>
        <MenuItemStyled onClick={handleClose}>My account</MenuItemStyled>
        <MenuItemStyled onClick={handleClose}>Logout</MenuItemStyled>
      </Menu>
    </Container>
  );
};
