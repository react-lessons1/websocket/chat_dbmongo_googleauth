import { AppBar, Box, Toolbar, styled } from '@mui/material';
import { useContext } from 'react';

import { LoginDialog } from '../Account/LoginDialog';
import { AccountContext } from 'src/context/AccountProvider';
import { Chat } from '../Chat/Chat';

const Component = styled(Box)`
  height: 100vh;
  background-color: #dcdcdc;
`;

const Header = styled(AppBar)`
  height: 120px;
  background-color: #00bfa5;
  box-shadow: none;
`;

const LoginHeader = styled(AppBar)`
  height: 220px;
  background-color: #00bfa5;
  box-shadow: none;
`;

export const Messenger = () => {
  // get user data from context
  const { account } = useContext(AccountContext);

  return (
    <Component>
      {account ? (
        <>
          <Header>
            <Toolbar></Toolbar>
          </Header>
          <Chat />
        </>
      ) : (
        <>
          <LoginHeader>
            <Toolbar></Toolbar>
          </LoginHeader>
          <LoginDialog />
        </>
      )}
    </Component>
  );
};
