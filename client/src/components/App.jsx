import { Outlet } from 'react-router-dom';
import { GoogleOAuthProvider } from '@react-oauth/google';
import { clientId } from 'src/api/constants';
import { AccountProvider } from 'src/context/AccountProvider';

export const App = () => {
  // clientId for Google authorization
  return (
    <GoogleOAuthProvider clientId={clientId}>
      <AccountProvider>
        <Outlet />
      </AccountProvider>
    </GoogleOAuthProvider>
  );
};
