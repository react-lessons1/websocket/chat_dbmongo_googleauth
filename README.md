lesson = 'https://www.youtube.com/watch?v=95jrbQNlpzM&list=PL_6klLfS1WqHvChQckAurBlF5P2V7oQj-&index=2'

for googleAuthorization:

1. go to - https://console.cloud.google.com/getting-started?hl=ru&pli=1
2. Create new project
3. APIs & Services - Credentials - create OAuth 2.0 Client IDs - External
4. Edit App information
5. Add Test users
6. APIs & Services - Credentials - create OAuth 2.0 Client IDs
7. Application type - Web application
8. Authorized JavaScript origins - add URLs http://localhost, http://localhost:3000
9. Authorized redirect URIs - add URLs http://localhost, http://localhost:3000

### Server:

1. express
2. cors
3. nodemon
4. socket.io
5. morgan
6. body-parser
7. dotenv

### Socket server:

1. socket.io
2. nodemon

### Client:

1. socket.io-client
2. react-router-dom
3. axios
4. timeago-react - for to format the DateTime output in chat message
5. react-input-emoji
6. jwt-decode

### history

1. ver.4.0 - chat with DB mongo
